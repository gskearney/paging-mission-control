const fs = require("fs");

function formatDates(oldReading, currReading) {
    const oldDay = oldReading.split(" ")[0];
    const oldTime = oldReading.split(" ")[1].substring(0,5).replace(":",".");
    const currDay = currReading.split(" ")[0];
    const currTime = currReading.split(" ")[1].substring(0,5).replace(":",".");
    return [oldDay, oldTime, currDay, currTime];
}

var JSONAlerts = [];
function createJSONAlert(severityStr, readingArr, firstReading) {
    const dtStr = firstReading.slice(0,4) + '-' + firstReading.slice(4,6) + '-' + firstReading.slice(6,8) + 'T' + firstReading.slice(9) + 'Z';
    const alertStr = '{' + 
    '"satelliteId":' + readingArr[1] + ',"severity":"' + severityStr + 
    '","component":"' + readingArr[7] + '","timestamp":"' + dtStr + '"' + '}';
    JSONAlerts.push(alertStr);
}

// implementation from geeksforgeeks then modified
class Queue {
    constructor() {
        this.items = {};
        this.frontIndex = 0;
        this.backIndex = 0;
    }
    enqueue(item) {
        this.items[this.backIndex] = item;
        this.backIndex++;
        return item + ' inserted';
    }
    dequeue() {
        const item = this.items[this.frontIndex];
        delete this.items[this.frontIndex];
        this.frontIndex++;
        return item;
    }
    peek() {
        return this.items[this.frontIndex];
    }
    get length() {
        return this.backIndex - this.frontIndex;
    }
    get printQueue() {
        return this.items;
    }
}

const [redLowBatt1000Queue, redLowBatt1001Queue, redHighTstat1000Queue, redHighTstat1001Queue] = [new Queue(), new Queue(), new Queue(), new Queue()];

const buffer = new Buffer.alloc(1024);
fs.open('input.txt', 'r', function (err, fd) {
    if (err) {
        return console.error(err);
    }
 
    fs.read(fd, buffer, 0, buffer.length,
        0, function (err, bytes) {
            if (err) {
                console.log(err);
            }
 
            if (bytes > 0) {
                const bytesArr = buffer.slice(0, bytes);
                const inputStr = bytesArr.toString();
                const inputArr = inputStr.split('\n');

                // iterate through bytes array
                for (let i = 0; i < inputArr.length; i++) {                    
                    const readingArr = inputArr[i].split('|');

                    // RED LOW BATT
                    if (readingArr[6] < readingArr[5] && readingArr[7] == 'BATT') {
                        
                        if (readingArr[1] == 1000) {
                            if (redLowBatt1000Queue.length < 2) {
                                redLowBatt1000Queue.enqueue(readingArr[0]);
                            }
                            else {
                                // 2 red lows in queue
                                const [currReading, oldReading] = [readingArr[0], redLowBatt1000Queue.peek()];

                                var formattedDates = formatDates(oldReading, currReading);
                                const [oldDay, oldTime, currDay, currTime] = [formattedDates[0], formattedDates[1], formattedDates[2], formattedDates[3]];
    
                                if (oldDay == currDay && currTime - oldTime <= 0.05) {
                                    createJSONAlert("RED LOW", readingArr, oldReading);
                                }
                                redLowBatt1000Queue.dequeue();
                                redLowBatt1000Queue.enqueue(currReading);
                            }
                        }
                        else {
                            if (redLowBatt1001Queue.length < 2) {
                                redLowBatt1001Queue.enqueue(readingArr[0]);
                            }
                            else {
                                // 2 red lows in queue
                                const [currReading, oldReading] = [readingArr[0], redLowBatt1001Queue.peek()];

                                var formattedDates = formatDates(oldReading, currReading);
                                const [oldDay, oldTime, currDay, currTime] = [formattedDates[0], formattedDates[1], formattedDates[2], formattedDates[3]];
    
                                if (oldDay == currDay && currTime - oldTime <= 0.05) {
                                    createJSONAlert("RED LOW", readingArr, oldReading);
                                }
                                redLowBatt1001Queue.dequeue();
                                redLowBatt1001Queue.enqueue(currReading);
                            }
                        }
                    }

                    // RED HIGH TSTAT
                    if (readingArr[6] - readingArr[2] > 0 && readingArr[7] == 'TSTAT') {
                        
                        if (readingArr[1] == 1000) {
                            if (redHighTstat1000Queue.length < 2) {
                                redHighTstat1000Queue.enqueue(readingArr[0]);
                            }
                            else {
                                // 2 red highs in queue
                                const [currReading, oldReading] = [readingArr[0], redHighTstat1000Queue.peek()];

                                var formattedDates = formatDates(oldReading, currReading);
                                const [oldDay, oldTime, currDay, currTime] = [formattedDates[0], formattedDates[1], formattedDates[2], formattedDates[3]];
    
                                if (oldDay == currDay && currTime - oldTime <= 0.05) {
                                    createJSONAlert("RED HIGH", readingArr, oldReading);
                                }
                                redHighTstat1000Queue.dequeue();
                                redHighTstat1000Queue.enqueue(currReading);
                            }
                        }
                        else {
                            if (redHighTstat1001Queue.length < 2) {
                                redHighTstat1001Queue.enqueue(readingArr[0]);
                            }
                            else {
                                // 2 red highs in queue
                                const [currReading, oldReading] = [readingArr[0], redHighTstat1001Queue.peek()];

                                var formattedDates = formatDates(oldReading, currReading);
                                const [oldDay, oldTime, currDay, currTime] = [formattedDates[0], formattedDates[1], formattedDates[2], formattedDates[3]];
    
                                if (oldDay == currDay && currTime - oldTime <= 0.05) {
                                    createJSONAlert("RED HIGH", readingArr, oldReading);
                                }
                                redHighTstat1001Queue.dequeue();
                                redHighTstat1001Queue.enqueue(currReading);
                            }
                        }
                    }
                }
            }
            
            // Output JSON
            var alerts = '';
            for (var alert of JSONAlerts) {
                alerts = alerts + alert + ','
            }
            alerts = alerts.slice(0,alerts.length-1);
            alerts = '[' + alerts + ']';
            const alertsObj = JSON.parse(alerts);
            console.log(alertsObj);
 
            fs.close(fd, function (err) {
                if (err) {
                    console.log(err);
                }
            });
        });
});